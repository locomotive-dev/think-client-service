<?php
error_reporting(0);
ini_set('display_errors', 0);

include_once dirname(__FILE__).'/config.php';

foreach ($modules as $module)
{
	include_once THINK_CLIENT_ROOT_PATH.'/modules/core/'. $module .'.php';
	$module_config = THINK_CLIENT_ROOT_PATH.'/modules/local/'.basename($module).'Local.php';

	if (file_exists($module_config)) {
		include_once $module_config;
		$module = $module.'Local';
	}

	$GLOBALS['THINK_CLIENT_MODULES'][$module] = new $module();	
}

function think_client_html_replace($html)
{
	foreach ($GLOBALS['THINK_CLIENT_MODULES'] as $module_name => $module)
	{
        if (method_exists($module, 'replace_html')) {
            $module->setHtml($html);
            $html = $module->replace_html();
        }
    }

    $html = str_replace('</body>', '<!-- think_on --></body>', $html);
    return $html;
}