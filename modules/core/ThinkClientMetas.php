<?php
/**
 * Модуль для подмены данных
 */
class ThinkClientMetas extends ThinkClientHtmlMethods
{
	private $html;

	public function setHtml($html)
	{
		$this->html = $html;
	}

	public function replace_html()
    {
        if (file_exists($GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data']))
        {
            $static_data = require $GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data'];

            if(array_key_exists($GLOBALS['THINK_CLIENT_CONFIG']['page_url'], $static_data)) {
            	if(!empty($static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['h1']))
            		$this->html = $this->replace_h1($this->html, $static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['h1']);
            	if(!empty($static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['title']))
            		$this->html = $this->replace_title($this->html, $static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['title']);
            	if(!empty($static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['desc']))
            		$this->html = $this->replace_desc($this->html, $static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['desc']);
            	if(!empty($static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['content']))
            		$this->html = $this->replace_content($this->html, $static_data[$GLOBALS['THINK_CLIENT_CONFIG']['page_url']]['content']);
            }
        }

        return $this->html;
    }
}