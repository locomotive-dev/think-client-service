<?php
/**
 * Модуль для генерации данных
 */
class ThinkClientReplace extends ThinkClientHtmlMethods
{
	private $html;

	public function setHtml($html)
	{
		$this->html = $html;
	}

	public function replace_html()
    {
    	return $this->html;
    }
}