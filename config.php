<?php
define('THINK_CLIENT_ROOT_PATH', rtrim(realpath(dirname(__FILE__)), '/'));

/**
* Key Project
*/
$GLOBALS['THINK_CLIENT_CONFIG']['KEY'] = "Client B set_your_key";

/**
* Load classes
*/
define('THINK_CLIENT_PATH_CLASSES',  THINK_CLIENT_ROOT_PATH.'/src/classes');

spl_autoload_register(function ($class) {
	if(in_array($class, ['ThinkClientHtmlMethods', 'ThinkClientSync'])){
		include THINK_CLIENT_PATH_CLASSES. '/' . $class . '.php';
	}else{
		require_once implode('/', explode('_', $class)).'.php';
	}
});

if (!isset($GLOBALS['THINK_CLIENT_CONFIG'])) {
    $GLOBALS['THINK_CLIENT_CONFIG'] = array();
}

$GLOBALS['THINK_CLIENT_CONFIG']['page_uri'] = $_SERVER['REQUEST_URI'];
$GLOBALS['THINK_CLIENT_CONFIG']['page_url'] = '//'.$_SERVER['HTTP_HOST'].$GLOBALS['THINK_CLIENT_CONFIG']['page_uri'];
$GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'] = 'utf-8';

$GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data'] = THINK_CLIENT_ROOT_PATH.'/data/think_meta.cache.php';
$GLOBALS['THINK_CLIENT_CONFIG']['connection_ad'] = THINK_CLIENT_ROOT_PATH.'/data/ad.txt';

if (!is_file($GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data'])) {
	touch($GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data']);
}
if (is_file($GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data'])) {
	if (!is_writable($GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data'])) {
		chmod($GLOBALS['THINK_CLIENT_CONFIG']['cache_meta_data'], 0777);
	}
}

/**
* Content comment replace
*/
$GLOBALS['THINK_CLIENT_CONFIG']['content_replace'] = '#(<think_text[^>]*>)(.*?)(<\/think_text>)#is';

/**
* Modules include
*/
$modules = array(
	#ThinkClientReplace::class,
	ThinkClientMetas::class
);

?>