<?php

class ThinkClientHtmlMethods
{
	public function replace_h1($html, $h1)
    {
    	if (false === strpos($GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'], 'utf')
    		&& function_exists('iconv')) {
    		$h1 = iconv('utf-8', $GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'].'//IGNORE', $h1);
    	}

	    $new_h1 = '';
	    if (isset($h1)) {
	    	$find = preg_match('#<h1([^>]*)>(.(?!</h1>))*?.?</h1>#is', $html, $current_h1_pregs);
	    	if (!isset($current_h1_pregs[1])) {
	    		$current_h1_pregs[1] = '';
	    	}

	    	if (false !== $h1) {
	    		$new_h1 = '<h1'.$current_h1_pregs[1].'>'.$h1.'</h1>';
	    	}

	    	if ($find) {
	    		$html = str_replace($current_h1_pregs[0], $new_h1, $html);
	    	}
	    }

	    return $html;
	}

	public function replace_title($html, $title)
	{
		if (false === strpos($GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'], 'utf') && function_exists('iconv')) {
			$title = iconv('utf-8', $GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'].'//IGNORE', $title);
		}

	   	$new_title = '';
		if (isset($title)) {
			$find = preg_match('#<title([^>]*)>(.*?)</title>#is', $html, $current_title_pregs);
			if ($find) {
				$new_title = '<title'.$current_title_pregs[1].'>'.$title.'</title>';
				$html = str_replace($current_title_pregs[0], $new_title, $html);
			} else {
				$new_title = '<title>'.$title.'</title>';
				$html = str_replace('</head>', $new_title.'</head>', $html);
			}
		}

		return $html;
	}

	public function replace_desc($html, $description)
	{
		if (false === strpos($GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'], 'utf')
				&& function_exists('iconv')) {
				$description = iconv('utf-8', $GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'].'//IGNORE', $description);
		}

		$new_meta_description = '';
		if (isset($description)) {

			if (false !== $description) {
				$new_meta_description = '<meta name="description" content="'.$description.'" />';
			}

			if (preg_match("#<meta[^>]*name *= *[\"']description[\"'][^>]*content *= *[\"']([^\"']*)[\"'][^>]*>#is", $html, $current_description_pregs)) {
				$html = str_replace($current_description_pregs[0], $new_meta_description, $html);
			} else {
				$html = str_replace('</head>', $new_meta_description.'</head>', $html);
			}
		}

		return $html;
	}

	public function replace_content($html, $content)
	{
		if (empty($content)) {
			return $html;
		}

		if (false === strpos($GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'], 'utf') && function_exists('iconv')) {
			$content = iconv('utf-8', $GLOBALS['THINK_CLIENT_CONFIG']['site_default_encoding'].'//IGNORE', $content);
		}

		if (isset($GLOBALS['THINK_CLIENT_CONFIG']['content_replace']) && !empty($content)) {
			if (preg_match($GLOBALS['THINK_CLIENT_CONFIG']['content_replace'], $html, $pregs)) {

				if (4 == sizeof($pregs)) {
					$replace_to = $pregs[1].$content.$pregs[3];
				} else {
					$replace_to = $content;
				}

				$html = str_replace($pregs[0], $replace_to, $html);
			}
		}

		return $html;
	}
}